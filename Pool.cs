using System.Collections.Generic;
using UnityEngine;

public interface IObjectPool
{
    public Pool AmmoPool { get; }
}

[System.Serializable]
public class Pool
{
    [SerializeField] private int _identity;
    [SerializeField] private PoolItem _poolItem;
    [SerializeField] private int _poolSize;
    [SerializeField] private bool _autoExpand;

    private Queue<PoolItem> _poolQueue;

    public Transform Parent { get; set; }

    public void Register()
    {
        if (_poolQueue == null)
        {
            _poolQueue = new Queue<PoolItem>();
        }

        _identity = _poolItem.GetInstanceID();

        _poolItem.SetIdentity(_identity);
        _poolItem.gameObject.SetActive(false);

        PoolManager.Instance.AddPool(this);
    }

    public void Deregister()
    {
        if (_poolQueue != null)
        {
            _poolQueue.Clear();
            _poolQueue = null;
        }

        PoolManager.Instance.RemovePool(this);
    }

    public PoolItem Lease(Vector3 position, Quaternion rotation)
    {
        if (_poolQueue.Count > 0)
        {
            PoolItem poolItem = _poolQueue.Dequeue();

            poolItem.transform.position = position;
            poolItem.transform.rotation = rotation;

            poolItem.gameObject.SetActive(true);

            return poolItem;
        }

        if (_autoExpand)
        {
            PoolItem poolItem = PoolItem.Instantiate(_poolItem, Parent);

            poolItem.transform.name = _poolItem.name + "-PoolItem";

            poolItem.transform.position = position;
            poolItem.transform.rotation = rotation;

            poolItem.gameObject.SetActive(true);

            return poolItem;
        }

        return null;
    }

    public void Revoke(PoolItem poolItem)
    {
        poolItem.gameObject.SetActive(false);

        poolItem.transform.position = Vector3.zero;
        poolItem.transform.rotation = Quaternion.identity;

        _poolQueue.Enqueue(poolItem);
    }

    public int GetIdentity()
    {
        return _identity;
    }

    public Queue<PoolItem> GetPoolQueue()
    {
        return _poolQueue;
    }

    public PoolItem GetPoolItem()
    {
        return _poolItem;
    }

    public int GetPoolSize()
    {
        return _poolSize;
    }
}
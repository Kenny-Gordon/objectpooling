using UnityEngine;

public class PoolItem : MonoBehaviour
{
    [SerializeField] private int _identity = 0;
    [SerializeField] private bool _autoRevoke = true;
    [SerializeField] private float _revokeTime = 30.0f;
    private float _elapsedTime;

    // This function is called when the object becomes enabled and active
    private void OnEnable()
    {
        _elapsedTime = 0;
    }

    // Update is called every frame, if the MonoBehaviour is enabled
    private void Update()
    {
        if (_autoRevoke)
        {
            _elapsedTime += Time.deltaTime;

            if (_elapsedTime >= _revokeTime)
            {
                Revoke();
            }
        }
    }

    public void Revoke()
    {
        Pool pool = PoolManager.Instance.FetchPool(_identity);
        pool.Revoke(this);
    }

    public void SetIdentity(int id)
    {
        _identity = id;
    }
}
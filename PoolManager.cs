using System.Collections.Generic;
using UnityEngine;

public class PoolManager : Singleton<PoolManager>
{
    private Dictionary<int, Pool> _poolDictionary;

    public void Awake()
    {
        _poolDictionary = new Dictionary<int, Pool>();
    }

    public void AddPool(Pool pool)
    {
        if (_poolDictionary.ContainsKey(pool.GetIdentity()))
        {
            Debug.LogWarning("Pool with ID: [" + pool.GetIdentity() + "] already exists!");

            return;
        }

        // Create the PoolHolder 
        GameObject poolHolder = new GameObject();

        poolHolder.transform.name = pool.GetPoolItem().name + "-Pool";
        poolHolder.transform.parent = transform;

        // Move the PoolHolder to the PoolManager
        pool.Parent = poolHolder.transform;

        // Fill the Pool with PoolItems
        for (int i = 0; i < pool.GetPoolSize(); i++)
        {
            PoolItem poolItem = Instantiate(pool.GetPoolItem(), pool.Parent);

            poolItem.transform.name = pool.GetPoolItem().name + "-PoolItem";

            poolItem.transform.position = Vector3.zero;
            poolItem.transform.rotation = Quaternion.identity;

            pool.GetPoolQueue().Enqueue(poolItem);
        }

        _poolDictionary.Add(pool.GetIdentity(), pool);
    }

    public void RemovePool(Pool pool)
    {
        if (_poolDictionary.ContainsKey(pool.GetIdentity()))
        {
            _poolDictionary.Remove(pool.GetIdentity());
            Destroy(pool.Parent);
        }
    }

    public Pool FetchPool(int id)
    {
        if (!_poolDictionary.TryGetValue(id, out Pool pool))
        {
            Debug.LogWarning("Pool with ID: [" + id + "] doesn't exists!");

            return null;
        }
        return pool;
    }
}